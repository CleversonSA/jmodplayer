/** 
 *  JModPlayer Front-end is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Author: Cleverson S A
 */

#include <LiquidCrystal.h>
#include <Math.h>

/** ------------------------------ GLOBALS -------------------------------------------**/
// initialize the LCD PINS
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
int LCD_COLS = 16;
int LCD_ROWS = 2;

// Set the 74HC95M Shift-out CI pins for LED's output
byte SO_DS_PIN=8;
byte SO_STCP_PIN=9;
byte SO_SHCP_PIN=10;

// Set the CD402183 Shift-in CI for the input buttons.
// because the Arduino's sync method, two bits are wasted for 
// Sync control, so you could use at least 6 buttons
byte SI_LATCH_PIN = 6;
byte SI_DATA_PIN = 13;
byte SI_CLOCK_PIN = 7;

// Define variables to hold the data for shift register.
// starting with a non-zero numbers can help troubleshoot
// Thanks for: https://www.arduino.cc/en/Tutorial/ShftIn12
byte si_switch_var1 = 72;  //01001000

// Define an array that corresponds to values for each 
// of the shift register's (IN) pins
byte BUTTONS=8;
char button_ids[] = {'N', 'A', 'B', 'C', 'D', 'E', 'F', 'N'}; 


void setup() {
  Serial.begin(9600);
  delay(100);
  
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);

  // initializes the shift out ci, some leds will blink
  pinMode(SO_DS_PIN  , OUTPUT);
  pinMode(SO_STCP_PIN, OUTPUT);
  pinMode(SO_SHCP_PIN, OUTPUT);
  sendToShiftOutCI(0xFF);
  delay(1000);
  sendToShiftOutCI(0x00);

  //define pin modes
  pinMode(SI_LATCH_PIN, OUTPUT);
  pinMode(SI_CLOCK_PIN, OUTPUT); 
  pinMode(SI_DATA_PIN, INPUT);

  Serial.print("READY");

}

/**
 * Sequence and write the register array bits states to the SN74HC595N
 * Thanks for: https://blog.3d-logic.com/2014/06/14/when-14-pins-is-not-enough/
 */
void sendToShiftOutCI(byte value){
  digitalWrite(SO_STCP_PIN, LOW);

  shiftOut(SO_DS_PIN, SO_SHCP_PIN, MSBFIRST, value);

  digitalWrite(SO_STCP_PIN, HIGH);
}


/**
 * Returns a byte with each bit in the corresponding to a pin on
 * the shift register. leftBit 7 = Pin 7 / Bit 0= Pin 0
 * Thanks:  https://www.arduino.cc/en/Tutorial/ShftIn12
 */
int shiftIn(int myDataPin, int myClockPin) { 
  int i;
  int temp = 0;
  int pinState;
  byte myDataIn = 0;

  pinMode(myClockPin, OUTPUT);
  pinMode(myDataPin , INPUT);
  
  //we will be holding the clock pin high 8 times (0,..,7) at the
  //end of each time through the for loop
  
  //at the begining of each loop when we set the clock low, it will
  //be doing the necessary low to high drop to cause the shift
  //register's DataPin to change state based on the value
  //of the next bit in its serial information flow.
  //The register transmits the information about the pins from pin 7 to pin 0
  //so that is why our function counts down
  
  for (i=7; i>=0; i--)
  {
    digitalWrite(myClockPin, 0);
    delayMicroseconds(0.2);
    temp = digitalRead(myDataPin);
    if (temp) {
      pinState = 1;
      //set the bit to 0 no matter what
      myDataIn = myDataIn | (1 << i);
    }
    else {
      //turn it off -- only necessary for debuging
     //print statement since myDataIn starts as 0
      pinState = 0;
    }
    
    digitalWrite(myClockPin, 1);
  }

  return myDataIn;
}


/**
 * Identifies if and wich button is pressed,
 * returns -1 if none is pressed
 * Based on:  https://www.arduino.cc/en/Tutorial/ShftIn12
 */
int identifyPressedButton(){
  
  //Pulse the latch pin:
  //set it to 1 to collect parallel data
  digitalWrite(SI_LATCH_PIN,1);
  //set it to 1 to collect parallel data, wait
  delayMicroseconds(100);
  //set it to 0 to transmit data serially  
  digitalWrite(SI_LATCH_PIN,0);

  //while the shift register is in serial mode
  //collect each shift register into a byte
  //the register attached to the chip comes in first 
  si_switch_var1 = shiftIn(SI_DATA_PIN, SI_CLOCK_PIN);

  for (int n=0; n<BUTTONS; n++)
  {
    //so, when n is 3, it compares the bits
    //in switchVar1 and the binary number 00001000
    //which will only return true if there is a 
    //1 in that bit (ie that pin) from the shift
    //register.
    
    if (si_switch_var1 & (1 << n) ){
       return n;
    }
    
  }

  return -1;
}


/**
 * Sends the pressed button char to the Java with no Line Breaks
 */
void sendPressedButton(int button_pressed){
  Serial.print("B");
  Serial.print(button_ids[button_pressed]);
  delay(25);
}

/**
 * Identify the command received from java, from a byte
 * Return I if the reading action returned a problem;
 */
char identifyReceivedCommand(){
  byte bcmd = Serial.read();

  if (bcmd == -1) {
    return 'I'; //Ignore
  }

  return (char)bcmd;
}


/**
 * Display any message from java measuring the buffer size
 * with the LCD cols size
 */
void displayJavaMessage(){
  int max_reading_tries = 12;
  int j=0;
  char buffer[16]={' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '};
  
  Serial.readBytes(buffer,LCD_COLS);
  
  lcd.setCursor(0,0);
  for (int k=0; k<LCD_COLS; k++){
     lcd.print(buffer[k]);
  }
}


/**
 * Turn on of off the leds reading the bits data
 */
void turnOnOfLEDS(){
  char buffer[3] = {0,0,0};
  int factor=0;
  Serial.readBytes(buffer, 3);

  int equivalent = 0;
  for (int i=0; i<3; i++){
    switch((char)buffer[i]){
      case '0': factor=0; break;
      case '1': factor=1; break;
      case '2': factor=2; break;
      case '3': factor=3; break;
      case '4': factor=4; break;
      case '5': factor=5; break;
      case '6': factor=6; break;
      case '7': factor=7; break;
      case '8': factor=8; break;
      case '9': factor=9; break;
    }

    if (i == 0){
      factor = factor * 100;
    }else if (i== 1){
      factor = factor * 10;
    }
    
    equivalent = equivalent + factor;
  }
  
  
  sendToShiftOutCI((byte)equivalent);
 
}

void loop() {
  int button_pressed = identifyPressedButton();
  
  //Pressed button has priority to send to the java the information
  //at the turn
  if (button_pressed > -1){
     sendPressedButton(button_pressed);
     delay(250);
     return;
  }

 while(Serial.available()){
    
    //Identify the command received
    char cmd = identifyReceivedCommand();
    
    if (cmd == '@'){
      displayJavaMessage();
    }else if (cmd == '#'){
      turnOnOfLEDS();
    }else {
      lcd.setCursor(0,1);
      lcd.print("Waiting java... ");  
    }

    Serial.flush();
 }

}




